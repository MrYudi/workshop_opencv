# Operações comumente realizadas em imagens binárias
# Ao contrario do erosin, se ao menos 1 pixel sobre o kernel for 1, o pixel resultante é 1 também

import cv2
import numpy as np

img = cv2.imread("Base.png", 0)
kernel = np.ones((5,5), np.uint8)
dilatation = cv2.dilate(img, kernel, iterations = 5)

cv2.imshow("Normal", img)
cv2.imshow("Resultante", dilatation)

cv2.waitKey(0)
cv2.destroyAllWindows()
