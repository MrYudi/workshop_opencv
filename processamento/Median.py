# Computa o valor mediano de todos os pixeis sobre a janela do Kernel e substitui no pixel central.
# Efetivo contra ruidos salt and pepper.


import cv2

img = cv2.imread("Pepper.png", 0)
med = cv2.medianBlur(img, 5)

cv2.imshow("Normal", img)
cv2.imshow("Mediana", med)

cv2.waitKey(0)
cv2.destroyAllWindows()
