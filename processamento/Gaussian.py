# Em vez de um kernel de peso identico, um kernel gaussiano é utilizado
# Utiliza-se da função cv2.GaussianBlur()
# Deve ser determinado a largura e altura do kernel (positivo e impar)
# Deve ser determinado também o sigma (Desvio padrão), nas direções X e Y (sigmaX e sigmaY)
# Se apenas o sigmaX for designado, o outro será calculado
# Se ambos os sigmas forem zeros, eles serão calculados do tamanho do kernel
# Possivel criar um kernel Gaussiano com a função cv2.getGaussianKernel().
# Muito efetivo em remover ruido gaussiano

import cv2

img = cv2.imread("BIT.png")
Gauss = cv2.GaussianBlur(img, (9, 9), sigmaX = 1, sigmaY = 30) # Desvio padrão
# ENTENDER

cv2.imshow("Normal", img)
cv2.imshow("Blur", Gauss)

cv2.waitKey(0)
cv2.destroyAllWindows()
