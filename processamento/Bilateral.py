# Projetado para remover ruidos, porém preservando as bordas.
# Lento comparado com os outros filtros
# Filtro gaussiano por exemplo, não considera se o pixel possui quase a mesma intensidade, ou se localiza
# em uma borda

# Este modelo, utiliza de mais um filtro gaussiano (multiplicativo) que leva em consideração as diferenças
#entre as intensidades dos pixeis
import cv2

img = cv2.imread("Bil.png", 0)
Gauss = cv2.GaussianBlur(img, (9, 9), sigmaX = 1, sigmaY = 30)
bil = cv2.bilateralFilter(img, 9, 75, 75)

cv2.imshow("Normal", img)
cv2.imshow("Bilateral", bil)
cv2.imshow("Gauss", Gauss)

cv2.waitKey(0)
cv2.destroyAllWindows()
