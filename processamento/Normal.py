# Como qualquer sinal digital, é possível aplicar um filtro passa baixa, passa alta, e etc...
# LPF => Remover ruido e tirar o foco da imagem
# HPF => Detecção de arestas

# cv2.filter2D() => Convoluir um kernel com a imagem
# Cada pixel é calculado seu novo valor, com a soma dos pixeis vizinhos / quantidade de pixeis
import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread("Brazil_BASE.jpg", 0)
# Explicar criação do kernel
kernel = np.ones((5, 5), np.float32) / 25
# -1 Profundidade desejada (-1 profundidade da imagem original) (Depth = Precisão de cada pixel)
img_f = cv2.filter2D(img, -1, kernel)

cv2.imshow("Normal", img)
cv2.imshow("Kernelizada", img_f)

cv2.waitKey(0)
cv2.destroyAllWindows()
