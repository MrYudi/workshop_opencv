# Em alguns casos, se necessida de um kernel de forma eliptica ou circular.
# Para isto se utiliza a função cv2.getStructuringElement(), onde se passa a forma e o tamanho do kernel

import cv2

array1 = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
array2 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (20, 20))
array3 = cv2.getStructuringElement(cv2.MORPH_CROSS, (5, 5))

print(array1)
print(array2)
print(array3)
