# Operações comumente realizadas em imagens binárias
# Realizada uma operação de erosão seguida por dilatação (Útil para remover ruidos)
import cv2
import numpy as np

img = cv2.imread("Opening.png", 0)
kernel = np.ones((5,5), np.uint8)
opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)

cv2.imshow("Normal", img)
cv2.imshow("Resultante", opening)

cv2.waitKey(0)
cv2.destroyAllWindows()
