import cv2
from datetime import datetime
from sklearn.externals import joblib

# Aponta o dispositivo de vídeo e carrega o xml que descreve as faces
vobj = cv2.VideoCapture(0)
face_detector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
loaded_model = joblib.load('modelo_finalizado.sav')
embExtractor = cv2.dnn.readNetFromTorch('modelo_recog/nn4.small2.v1.t7')

label_dict = {0 : 'Augusto', 1 : 'Crispim', 2 : 'Stephanie'}

while True:
    status, frame = vobj.read() # Retorna o frame e se foi lido com sucesso
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # Converte para escala de cinza
    # Detecta faces nas imagens
    faces = face_detector.detectMultiScale(frame_gray, scaleFactor = 1.05, minNeighbors = 5)

    if len(faces) >= 1:
        for i, face in enumerate(faces):
            x, y, w, h = face
            # Converte a face para um blob
            faceBlob = cv2.dnn.blobFromImage(frame[y: y + h, x: x + w],
                        1.0 / 255, (96, 96), (0, 0, 0), swapRB = True, crop = False)
            # Extrai a assinatura da mesma
            embExtractor.setInput(faceBlob)
            signature = embExtractor.forward().flatten()
            # Desenha um retângulo, em volta da Face
            cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 3)
            label = loaded_model.predict(signature.reshape(1, -1))
            cv2.putText(frame, label_dict[label[0]], (x, y - 10) if y - 10 > 10 else (x, y + 10), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (255, 255, 255), lineType=cv2.LINE_AA) 

     # Exibe a imagem e aguard o tempo em ms, por uma tecla ser pressionada
    cv2.imshow("Output Camera", frame)
    key = cv2.waitKey(1) & 0xFF
 
    if key == ord('q'): # Se for q sai do while
        break

cv2.destroyAllWindows()
vobj.release()
