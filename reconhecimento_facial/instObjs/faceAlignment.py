# warpAffine, faz afffine transformatons (rotating, scaling, translating), o segredo é criar a matriz de rotação, ou qualquer que seja a operação
import numpy as np
import cv2

facialPointsDic = {"queixo":(0, 17), "sombrancelha_dir":(17, 22), "sombrancelha_esq":(22, 27), "nariz":(27, 35), "olho_dir":(36, 42), "olho_esq":(42, 48), "boca":(48, 68)}

class Face_Aligner:
    # leftEyePos = (x, y) especifica a posição do olho esquerdo na imagem resultante, default é entre 20 e 40%, que indica o zoom da Face (20% Sendo o zoom mais próximo)
    # faceWidth e faceHeight largura e altura da face em pixels
    def __init__(self, leftEyePos = (0.25, 0.25), faceWidth = 200, faceHeight = None):
        self.leftEyePos = leftEyePos
        self.faceWidth = faceWidth
        self.faceHeight = faceHeight
        if self.faceHeight is None:
            self.faceHeight = self.faceWidth
            
    def align(self, image, facesPoints):

        facesAligned = []

        for facePoints in facesPoints:
        
            #Extrai a posição dos olhos
            (lI, lF) = facialPointsDic["olho_esq"]
            (rI, rF) = facialPointsDic["olho_dir"]
            olhoEsqPts = facePoints[lI:lF]
            olhoDirPts = facePoints[rI:rF]
            
            #Calculo da centroide de cada olho e o angulho entre as centroides #############
            olhoEsqCent = olhoEsqPts.mean(axis = 0).astype("int")
            olhoDirCent = olhoDirPts.mean(axis = 0).astype("int")
            dX = olhoDirCent[0] - olhoEsqCent[0]
            dY = olhoDirCent[1] - olhoEsqCent[1]
            angulo = np.degrees(np.arctan2(dY, dX)) - 180
            
            #Calculo das coordenadas do olho direito baseado nas coordenadas do esquerdo, a escala da imagem, determinando a distancia entre os olhos (Da imagem, desejada)
            olhoDirXPos = 1.0 - self.leftEyePos[0]
            realDist = np.sqrt((dX ** 2) + (dY ** 2))
            aimedDist = (olhoDirXPos - self.leftEyePos[0])
            aimedDist *= self.faceWidth
            escala = aimedDist / realDist
            
            #Calculo do ponto central, matriz de rotação e atualizo os componentes de translação da matriz
            oCentro = ((olhoEsqCent[0] + olhoDirCent[0]) // 2, (olhoEsqCent[1] + olhoDirCent[1]) // 2)
            matrizR = cv2.getRotationMatrix2D(oCentro, angulo, escala)
            tX = self.faceWidth * 0.5
            tY = self.faceHeight * self.leftEyePos[1]
            matrizR[0, 2] += (tX - oCentro[0])
            matrizR[1, 2] += (tY - oCentro[1])
            
            #Transformação
            (w, h) = (self.faceWidth, self.faceHeight)
            facesAligned.append(cv2.warpAffine(image, matrizR, (w, h), flags = cv2.INTER_CUBIC))
            
        return facesAligned