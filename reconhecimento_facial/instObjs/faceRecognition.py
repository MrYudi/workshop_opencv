import cv2
import numpy as np
import pickle

class Face_Recognition:
    def __init__(self, encodingsPath, labelsPath):
        self.recognizer = pickle.loads(open(encodingsPath, "rb").read())
        self.lbls = pickle.loads(open(labelsPath, "rb").read())

    def recognizes(self, fEmbs):
        self.names = []
        self.confidences = []
        for i in range(0, len(fEmbs)):
            predictions = self.recognizer.predict_proba(fEmbs)[i]
            j = np.argmax(predictions)
            self.names.append(self.lbls.classes_[j])
            self.confidences.append(predictions[j])
        return self.names, self.confidences

    def drawRecog(self, img, fPositions):
        for i, fPos in enumerate(fPositions):
            xCenter = fPos[0] + int((fPos[2] - fPos[0]) / 2)
            yCenter = fPos[1] + int((fPos[3] - fPos[1]) / 2)
            # print(self.names)
            text = self.names[i]
            cv2.circle(img, (xCenter, yCenter), 2, (255, 0, 0), -1)
            yTxt = yCenter - 10 if yCenter - 10 > 10 else yCenter + 10
            cv2.putText(img, text, (xCenter, yTxt), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)