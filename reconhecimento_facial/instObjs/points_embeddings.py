import cv2
import dlib
from imutils import face_utils

class FacialSignature_Detector:
    def __init__(self, PathPoints, PathEmbeddings):
        self.ptPredictor = dlib.shape_predictor(PathPoints)
        self.embExtractor = cv2.dnn.readNetFromTorch(PathEmbeddings)

    def detectsPoints(self, img, faces):
        self.faces = faces
        self.facialPoints = []
        self.img = img
        imGray = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
        for i in range(0, len(self.faces)):
            facialPoint = self.ptPredictor(imGray, dlib.rectangle(faces[i][0], faces[i][1], faces[i][2], faces[i][3]))
            self.facialPoints.append(face_utils.shape_to_np(facialPoint))
        
        return self.facialPoints

    def drawPoints(self):
        for i in range(0, len(self.faces)):
            for (x, y) in self.facialPoints[i]:
                cv2.circle(self.img, (x, y), 2, (255, 0, 0), -1)

    def detectsEmbeddings(self, faces2emb, fCoord = True):
        # Recebendo coordenadas dos rostos, possível passar também o rosto em si (Caso dos rostos alinhados)
        vectors128D = []

        if len(faces2emb) != 0:
            if fCoord is True:
                for i in range(0, len(faces2emb)):
                    for j in range(0, 4):
                        if faces2emb[i][j] < 0:
                            faces2emb[i][j] = 0
    
                    faceBlob = cv2.dnn.blobFromImage(self.img[faces2emb[i][1]:faces2emb[i][3], faces2emb[i][0]:faces2emb[i][2]],
                        1.0 / 255, (96, 96), (0, 0, 0), swapRB = True, crop = False)
                    # cv2.imshow("Teste", self.img[faces2emb[i][1]:faces2emb[i][3], faces2emb[i][0]:faces2emb[i][2]])
                    self.embExtractor.setInput(faceBlob)
                    vectors128D.append(self.embExtractor.forward().flatten())
            elif fCoord is False:
                for i in range(0, len(faces2emb)):
                    faceBlob = cv2.dnn.blobFromImage(faces2emb[i], 1.0 / 255, (96, 96), (0, 0, 0), swapRB = True, crop = False)
                    # cv2.imshow("Teste", faces2emb[i])
                    self.embExtractor.setInput(faceBlob)
                    emb = self.embExtractor.forward()
                    vectors128D.append(emb.flatten())
        
        return vectors128D
