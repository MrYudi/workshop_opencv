import cv2
from datetime import datetime

# Aponta o dispositivo de vídeo e carrega o xml que descreve as faces
vobj = cv2.VideoCapture(0)
face_detector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
# Incia contador para tirar fotos a cada 'n' segundos
start_time = datetime.now()
n_photo = 0
save_pic = False

print(start_time)

while True:
    time_now = datetime.now()
    status, frame = vobj.read() # Retorna o frame e se foi lido com sucesso
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # Converte para escala de cinza
    # Detecta faces nas imagens
    faces = face_detector.detectMultiScale(frame_gray, scaleFactor = 1.05, minNeighbors = 5)

    if len(faces) >= 1:
        if (time_now - start_time).seconds > 1:
            save_pic = True
            n_photo += 1
            start_time = datetime.now()
        for i, face in enumerate(faces):
            x, y, w, h = face
            if save_pic is True:
                cv2.imwrite('dataset/{}_{}.jpg'.format(n_photo, i), frame[y: y + h, x: x + w])
            cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 7)
        save_pic = False

     # Exibe a imagem e aguard o tempo em ms, por uma tecla ser pressionada
    cv2.imshow("Output Camera", frame)
    key = cv2.waitKey(1) & 0xFF
 
    if key == ord('q'): # Se for q sai do while
        break
    

cv2.destroyAllWindows()
vobj.release()
